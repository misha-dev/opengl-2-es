package com.mmykytyn.airhockey;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.widget.Toast;

import com.mmykytyn.airhockey.utils.Logger;

/**
 * Created by Mike on 15.11.2015.
 */
public class AirHockeyActivity extends Activity {

    private static final String TAG = AirHockeyActivity.class.getSimpleName();
    GLSurfaceView glSurfaceView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Logger.d(TAG, "onCreate");

        glSurfaceView = new GLSurfaceView(this);

        final boolean supportsEs2 = ((ActivityManager)getSystemService(Context.ACTIVITY_SERVICE)).getDeviceConfigurationInfo().reqGlEsVersion >= 0x20000;

        if(!supportsEs2){
            Toast.makeText(this, "This device doesn't support OpenGL ES 2.0.",
                    Toast.LENGTH_LONG).show();
            finish();
        }else{
            glSurfaceView.setEGLContextClientVersion(2);
            glSurfaceView.setRenderer(new OpenGLRenderer(this));

            setContentView(glSurfaceView);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        glSurfaceView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        glSurfaceView.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
