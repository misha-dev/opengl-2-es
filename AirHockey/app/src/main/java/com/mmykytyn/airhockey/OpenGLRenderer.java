package com.mmykytyn.airhockey;

import android.app.Activity;
import android.opengl.GLSurfaceView;

import com.mmykytyn.airhockey.utils.Logger;
import com.mmykytyn.airhockey.utils.ShaderHelper;
import com.mmykytyn.airhockey.utils.TextResourceReader;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import static android.opengl.GLES20.GL_COLOR_BUFFER_BIT;
import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_LINES;
import static android.opengl.GLES20.GL_POINTS;
import static android.opengl.GLES20.GL_TRIANGLES;
import static android.opengl.GLES20.glClear;
import static android.opengl.GLES20.glClearColor;
import static android.opengl.GLES20.glDrawArrays;
import static android.opengl.GLES20.glEnableVertexAttribArray;
import static android.opengl.GLES20.glGetAttribLocation;
import static android.opengl.GLES20.glGetUniformLocation;
import static android.opengl.GLES20.glUniform4f;
import static android.opengl.GLES20.glUseProgram;
import static android.opengl.GLES20.glVertexAttribPointer;
import static android.opengl.GLES20.glViewport;


/**
 * Created by Mike on 10.11.2015.
 */
public class OpenGLRenderer implements GLSurfaceView.Renderer {

    private static final int BYTES_PER_FLOAT = 4;
    private static final int POSITION_COMPONENT_COUNT = 2;//z,y
    final private FloatBuffer vertexData;
    private final Activity activity;
    private int program;

    public static final String U_COLOR = "u_Color";
    private int uColorLocation;

    public static final String A_POSITION = "a_Position";
    private int aPositionLocation;

    final float[] tableVerticesWithTriangles = {
    //Triangle1
            -0.5f, -0.5f,
             0.5f,  0.5f,
            -0.5f,  0.5f,
    //Triangle2
            -0.5f, -0.5f,
             0.5f, -0.5f,
             0.5f,  0.5f,
    //Line1
            -0.5f, 0.0f,
             0.5f, 0.0f,
    //Mallets
            0f, -0.25f,
            0f, 0.25f
    };

    public OpenGLRenderer(Activity activity){
        this.activity = activity;

        vertexData = ByteBuffer
                .allocateDirect(tableVerticesWithTriangles.length * BYTES_PER_FLOAT)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer();

        vertexData.put(tableVerticesWithTriangles);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        glClearColor(1f,1f,0f,0f);

        String vertexShaderSource= TextResourceReader
                .readTextFileFromResource(activity, R.raw.simple_vertex_shader);
        String fragmentShaderSource=TextResourceReader
                .readTextFileFromResource(activity,R.raw.simple_fragment_shader);

        int vertexShader = ShaderHelper.compileVertexShader(vertexShaderSource);
        int fragmentShader = ShaderHelper.compileFragmentShader(fragmentShaderSource);

        program = ShaderHelper.linkProgram(vertexShader,fragmentShader);

        if(Logger.ON){
            ShaderHelper.validateProgram(program);
        }

        glUseProgram(program);

        uColorLocation = glGetUniformLocation(program,U_COLOR);
        aPositionLocation = glGetAttribLocation(program,A_POSITION);

        vertexData.position(0);
        glVertexAttribPointer(aPositionLocation,POSITION_COMPONENT_COUNT,GL_FLOAT,false,0,vertexData);
        glEnableVertexAttribArray(aPositionLocation);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        glViewport(0,0,width,height);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        glClear(GL_COLOR_BUFFER_BIT);

        glUniform4f(uColorLocation, 1f, 1f, 1f, 1f); //Table
        glDrawArrays(GL_TRIANGLES,0,6);

        glUniform4f(uColorLocation,1f,0f,0f,1f); //Dividing line
        glDrawArrays(GL_LINES,6,2);

        glUniform4f(uColorLocation,0f,0f,1f,1f); //Dividing mallet
        glDrawArrays(GL_POINTS,8,1);

        glUniform4f(uColorLocation,1f,0f,0f,1f); //Dividing mallet
        glDrawArrays(GL_POINTS,9,1);


    }
}
