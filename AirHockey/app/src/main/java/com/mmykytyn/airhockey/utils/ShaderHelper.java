package com.mmykytyn.airhockey.utils;

import android.util.Log;

import static android.opengl.GLES20.GL_COMPILE_STATUS;
import static android.opengl.GLES20.GL_FRAGMENT_SHADER;
import static android.opengl.GLES20.GL_LINK_STATUS;
import static android.opengl.GLES20.GL_VALIDATE_STATUS;
import static android.opengl.GLES20.GL_VERTEX_SHADER;
import static android.opengl.GLES20.glAttachShader;
import static android.opengl.GLES20.glCompileShader;
import static android.opengl.GLES20.glCreateProgram;
import static android.opengl.GLES20.glCreateShader;
import static android.opengl.GLES20.glDeleteProgram;
import static android.opengl.GLES20.glDeleteShader;
import static android.opengl.GLES20.glGetProgramInfoLog;
import static android.opengl.GLES20.glGetProgramiv;
import static android.opengl.GLES20.glGetShaderInfoLog;
import static android.opengl.GLES20.glGetShaderiv;
import static android.opengl.GLES20.glLinkProgram;
import static android.opengl.GLES20.glShaderSource;
import static android.opengl.GLES20.glValidateProgram;

/**
 * Created by Mike on 14.11.2015.
 */
public class ShaderHelper {

    public static final String TAG = ShaderHelper.class.getSimpleName();

    public static int compileVertexShader(String shaderCode){
        return compileShader(GL_VERTEX_SHADER,shaderCode);
    }
    public static int compileFragmentShader(String shaderCode){
        return compileShader(GL_FRAGMENT_SHADER,shaderCode);
    }
    private static int compileShader(int type,String shaderCode){
        int shader = glCreateShader(type);

        if(shader == 0){
            Logger.e(TAG,"couldn't create shader");
        }else{

            glShaderSource(shader,shaderCode);
            glCompileShader(shader);

            int[] compileStatus = new int[1];
            glGetShaderiv(shader,GL_COMPILE_STATUS,compileStatus,0);

            if(Logger.ON){
                String infoLog = glGetShaderInfoLog(shader);
                Logger.d(TAG,"Shader compilation info : shader="+shaderCode+", result:"+infoLog);
            }

            if(compileStatus[0] == 0){
                glDeleteShader(shader);
                Logger.e(TAG,"shader compilation failed, shader="+shader);
                shader = 0;
            }

        }

        return shader;
    }

    public static final int linkProgram(int vertexShader, int fragmentShader){

        int programShaderId = glCreateProgram();

        if(programShaderId == 0){
            Logger.e(TAG,"Couldn't create new program");
        }else{

            glAttachShader(programShaderId,vertexShader);
            glAttachShader(programShaderId,fragmentShader);

            glLinkProgram(programShaderId);

            int[] linkStatus = new int[1];
            glGetProgramiv(programShaderId,GL_LINK_STATUS,linkStatus,0);

            String programInfoLog = glGetProgramInfoLog(programShaderId);

            Log.d(TAG,"Results of linking program : "+programInfoLog);

            if(linkStatus[0] == 0){
                glDeleteProgram(programShaderId);
                programShaderId = 0;

                Log.e(TAG,"Linking of program failed");
            }

        }

        return programShaderId;
    }

    public static boolean validateProgram(int programId){
        glValidateProgram(programId);

        int[] validateStatus = new int[1];

        glGetProgramiv(programId,GL_VALIDATE_STATUS,validateStatus,0);

        Logger.d(TAG,"Results of validating program: "+validateStatus[0]+" Log:"+glGetProgramInfoLog(programId));

        return validateStatus[0] != 0;
    }

}
