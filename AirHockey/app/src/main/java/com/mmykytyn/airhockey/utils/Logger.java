package com.mmykytyn.airhockey.utils;

import android.util.Log;

/**
 * Created by Mike on 14.11.2015.
 */
public class Logger {

    final public static boolean ON = true;

    public static final void d(String tag, String message){
        if(ON)
            Log.d(tag,message);
    }

    public static final void e(String tag, String message){
        if(ON)
            Log.e(tag,message);
    }

    public static final void e(String tag, Exception e){
        if(ON)
            Logger.e(tag,e);
    }

}
