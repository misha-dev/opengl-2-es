package com.mmykytyn.blank;

import android.app.ActivityManager;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.Toast;


public class OpenGLActivity extends ActionBarActivity {

    GLSurfaceView glSurfaceView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        glSurfaceView = new GLSurfaceView(this);

        final boolean supportsEs2 = ((ActivityManager)getSystemService(Context.ACTIVITY_SERVICE)).getDeviceConfigurationInfo().reqGlEsVersion >= 0x20000;

        if(!supportsEs2){
            Toast.makeText(this,"This device doesn't support OpenGL ES 2.0.",
                    Toast.LENGTH_LONG).show();
            finish();
        }else{
            glSurfaceView.setEGLContextClientVersion(2);
            glSurfaceView.setRenderer(new OpenGLProjectRenderer());

            setContentView(glSurfaceView);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        glSurfaceView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        glSurfaceView.onPause();
    }
}
